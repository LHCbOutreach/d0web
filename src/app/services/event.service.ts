/**
* (c) Copyright 2019 CERN                                                     
*                                                                             
* This software is distributed under the terms of the GNU General Public      
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   
*                                                                             
* In applying this licence, CERN does not waive the privileges and immunities 
* granted to it by virtue of its status as an Intergovernmental Organization  
* or submit itself to any jurisdiction.                                       
*/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface Particle {
		E: number;
		chi2: number;
		ip: number;
		ipchi2: number;
		mass: number;
		particleId: string;
		pvx: number;
		pvy: number;
		pvz: number;
		px: number;
		py: number;
		pz: number;
		q: number;
		trajectory: number[][];
		zFirstMeasurement: number;
}

@Injectable()
export class EventService {

	constructor( private http: HttpClient) {}
	
	getevent( url ) {
		return this.http.get<Particle[]>( url );
	}
}

/**
* (c) Copyright 2019 CERN                                                     
*                                                                             
* This software is distributed under the terms of the GNU General Public      
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   
*                                                                             
* In applying this licence, CERN does not waive the privileges and immunities 
* granted to it by virtue of its status as an Intergovernmental Organization  
* or submit itself to any jurisdiction.                                       
*/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface D0 {
		MM: number;
		PT: number;
		TAU: number;
		MINIP: number;
}

@Injectable()
export class D0dataService {

	constructor( private http: HttpClient) {}

	getdata( url ) {
		return this.http.get<D0[]>( url );
	}
}

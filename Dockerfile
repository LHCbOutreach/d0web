# base image
FROM node:14 as build

# install chrome for protractor tests
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update && apt-get install -yq google-chrome-stable

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@9.1.12

# add app
COPY . /app

# start app
RUN ng build --prod --output-path=dist
# For the local development container, this would be:
# CMD ng serve --host 0.0.0.0 

############
### prod ###
############

# base image
FROM nginx:1.16.1-alpine

# copy artifact build from the 'build environment'
COPY --from=build /app/dist /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/nginx.conf
RUN sed -i.bak 's/listen.*80;/listen 8080;/'  /etc/nginx/conf.d/default.conf


# expose port 8080
EXPOSE 8080

# Change user...
USER nginx

# run nginx
CMD ["nginx", "-g", "daemon off;"]
